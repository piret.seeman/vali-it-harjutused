
// Ütleme, et meie peres on üks auto, mis on ühises kasutuses. Seega on Auto klass
// avalikult kättesaadav ka näiteks lastele.

public class Auto {

    // Mida meie autoga teha saab? Või mida peab tegema autoga sellega sõitmiseks?

    // Näiteks võib olla autot vaja selleks, et kooli sõita
    public static String s6idaKooli(boolean t88paev) {
        if (t88paev){
            return "Sõidan hommikul kooli ja õhtul koju tagas";
        } else {
            return "Täna ei ole vaja kooli minna!";
        }
    }

    // private static void

    public static boolean parkimine(int aeg) {
        if (aeg > 9 && aeg < 17) {
            return true;
        } else {
            return false;
        }

        //return (aeg > 9 && aeg < 17);
    }


    // Milline võiks olla näide tegevusest (meetodist), mis tagastab integeri?
    // Aga milline võiks olla näide meetodist, mis võtab sisendiks ainult Stringe?

}
