// Näidisprojekt, et aidata läbi mõelda, mis on Javas milleks hea

/* Et näide oleks võimalikult eluline, teeme nii, et see klass siin (Main.java fail) on igapäevaelu To Do listide
** kogumik. Nii nagu igapäevas on ka siin erinevaid valdkondi või asju, millega seonduvat tuleb eraldi läbi mõelda.
** Näiteks tuleb majandada oma autoga seonduvat (teeme seda auto.java failis), lapsi (lapsed.java)
** või õppetööd (kool.java).
**  */

public class Main {

    public static void main(String[] args) {
        System.out.println("Minu To Do list:");

        // See on igapäevaelu n-ö koondnimekiri asjadest, mida tuleb teha - mitte detailselt, aga
        // rohkem pealkirjatasemel

        // Miks me prindime tulemuse välja? Et saada endale nimekirja

        boolean t88paev = true;
        System.out.println("* " + Auto.s6idaKooli(t88paev));

        int aeg = 10;
        System.out.println("* Auto parkima? " + Auto.parkimine(aeg));


    }
}
