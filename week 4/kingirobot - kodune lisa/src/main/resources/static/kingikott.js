
var url = "http://localhost:8080/kingikott/"

var pealkiri = getQueryVariable("pealkiri")
document.querySelector("#pealkirjakoht").innerHTML = pealkiri

// Vajalik tabeli kuvamiseks
var refreshTabel = async function() {
    console.log("refreshtabel läks käima!")
    var pealkiri = getQueryVariable("pealkiri")
    console.log("pealkiri: " + pealkiri)
    document.querySelector("#pealkirjakoht").innerHTML = pealkiri
    // Sulgudes lisatud aadress
    var andmed = await fetch(url + "?pealkiri=" + pealkiri) //APIControlleris ArrayList nimekiri
    var tabelTagasi = await andmed.json() //sama, mis APIControlleris tabel
    console.log(tabelTagasi)

    var tabelHTMLi = "" //loome tühja stringi, hakkame loopiga iga soovi kohta täitma.
    for (var soov of tabelTagasi) {
        var nimi1 = soov.nimi
        var info1 = soov.info
        var hind1 = soov.hind
        var kogus1 = soov.kogus
        var link1 = soov.link
        tabelHTMLi += "Nimi: " + nimi1 + ", Info: " + info1 + ", Hind:" + hind1 + ", Kogus: " + kogus1 + ", Link:" + link1 + "<br>" //loob iga soovi kohta ühe stringi.
    }
    // kirjutame üle tabelHTMLi, et see kuvaks end tabelina, valdavalt html, aga ${} sees on js
    // thead == tabeli esimene rida ehk pealkirjad; tr == praeguses kontekstis tulpade pealkirja rida
    // tbody == tabeli sisu, mis tekib iga sooviga (ehk array elemendiga)
    // ${} sees olev .map funktsiooni toimib iga soovi kohta ning kutsub välja looSoov funktsiooni
    // looSoov funktsioon defineerib iga soovi elemendid ehk asetab iga soovi atribuudid õigetesse tabeli lahtritesse
    tabelHTMLi = `
        <table>
            <thead>
                <tr>
                    <th>Nimi</th>
                    <th>Info</th>
                    <th>Hind</th>
                    <th>Kogus</th>
                    <th>Link</th>
                </tr>
            </thead>
            <tbody>
                ${tabelTagasi.map(looSoov).join('')}
            </tbody>
        </table>
    `
    document.querySelector("#koht").innerHTML = tabelHTMLi //saadame ArrayListi sooviga tagasi brauserisse.
}

// ${} == muutuja nimi - selle sees info js-s. väljaspool htmlis
// tr == table row; td == table data
function looSoov(soov) {
    return `
       <tr>
          <td>${soov.nimi}</td>
          <td>${soov.info}</td>
          <td>${soov.hind}</td>
          <td>${soov.kogus}</td>
          <td><a href=${soov.link} target="_blank">${soov.link}</a></td>
       </tr>
    `
}

// Saab välja kutsuda alles pärast selle funktsiooni ära kirjeldamist
refreshTabel()

// Selleks, et listi nime kuvades jääks tühikud alles (urlis on ok kui asendub % märgiga)
function getQueryVariable(variable) {
   var query = window.location.search.substring(1);
   var vars = query.split("&");
   for (var i=0;i<vars.length;i++) {
     var pair = vars[i].split("=");
     if (pair[0] == variable) {
       return decodeURIComponent(pair[1]);
     }
   }
   alert('Query Variable ' + variable + ' not found');
 }

// Nupule vajutades liigub ajaloos sammu tagasi
var muudannimekirjaButton = document.querySelector('#muudanimekirja')
muudannimekirjaButton.onclick = function(event) {
    console.log("muudannimekirjaButton töötab!")
    history.back()
}
