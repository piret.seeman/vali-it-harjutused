package ee.kingirobot.troojahobune;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin
@RestController
public class APIController {

   @Autowired
   private JdbcTemplate jdbcTemplate;


   @PostMapping ("/hobune")
   // siin tuleb defineerida meetod, mille kaudu java konverteerib fetchis küsitud andmed
   // sulgudesse annotatsioon @RequestBody, et ta teaks, et ta oskaks JS-st päringut vastu võtta

   public void lisaSoov (@RequestBody Soov soov) { //lisaSoov - meetod, mille abil sisestame uue kingisoovi andmebaasi.
        System.out.println("lisa uus soov hakkab tööle");
        System.out.println("Soovi nimi " + soov.getNimi());
        //Roheline SQL-käsk. INSERT loob tulpade nimed. VALUES lisab konkreetse soovi vastavasse tulpa.

        String sqlKask = "INSERT INTO tabel (syndmuseNimi, nimi, info, hind, kogus, link) VALUES ('"+ soov.getSyndmuseNimi() + "', '"+ soov.getNimi() + "', '"+ soov.getInfo()+"','"+soov.getHind()+"', '"+soov.getKogus()+"','"+soov.getLink()+"');";

        jdbcTemplate.execute(sqlKask);
       System.out.println("Andmebaasi sisestamine õnnestus!");

    }

    // Saadab info tagasi js-i, et listi soove lisades kuvaks need ka välja
    @GetMapping ("/hobune") //get päring
    public ArrayList<Soov> prindiV2lja(@RequestParam String pealkiri) {
        System.out.println("Prindi välja käivitus!");
        ArrayList<Soov> nimekiri = new ArrayList<>(); //Arraylisti sisse tulevad tabel(id) SQL-ist
        System.out.println("Pealkiri Getmappingus: " + pealkiri);
        nimekiri = (ArrayList) jdbcTemplate.queryForList("SELECT * FROM tabel WHERE syndmuseNimi= '"+ pealkiri +"'");
        return nimekiri;
    }


    // Saadab info tagasi js-i kingikoti htmli kuvamiseks
    @GetMapping ("/kingikott") //get päring
    public ArrayList<Soov> prindiV2lja2(@RequestParam String pealkiri) {
        System.out.println("Prindi välja käivitus!");
        ArrayList<Soov> nimekiri = new ArrayList<>(); //Arraylisti sisse tulevad tabel(id) SQL-ist
        System.out.println("Pealkiri Getmappingus: " + pealkiri);
        nimekiri = (ArrayList) jdbcTemplate.queryForList("SELECT * FROM tabel WHERE syndmuseNimi= '"+ pealkiri +"'");
        return nimekiri;
    }
}
