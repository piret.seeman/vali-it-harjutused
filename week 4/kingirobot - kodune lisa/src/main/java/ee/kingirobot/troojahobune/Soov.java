package ee.kingirobot.troojahobune;

public class Soov {

    private String syndmuseNimi;
    private String nimi;
    private String info;
    private String hind;
    private String kogus;
    private String link;

    public Soov() {

    }

    public String getNimi() {
        return nimi;
    }

    public String getInfo() {
        return info;
    }

    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getHind() {
        return hind;
    }

    public void setHind(String hind) {
        this.hind = hind;
    }

    public String getKogus() {
        return kogus;
    }

    public void setKogus(String kogus) {
        this.kogus = kogus;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getSyndmuseNimi() {
        return syndmuseNimi;
    }

    public void setSyndmuseNimi(String syndmuseNimi) {
        this.syndmuseNimi = syndmuseNimi;
    }
}
