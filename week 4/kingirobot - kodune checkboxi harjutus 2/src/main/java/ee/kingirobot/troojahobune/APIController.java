package ee.kingirobot.troojahobune;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin
@RestController
public class APIController {

   @Autowired
   private JdbcTemplate jdbcTemplate;


   // muudetud >>
   // POST /syndmus
   @PostMapping ("/syndmus")
   // siin tuleb defineerida meetod, mille kaudu java konverteerib fetchis küsitud andmed
   // sulgudesse annotatsioon @RequestBody, et ta teaks, et ta oskaks JS-st päringut vastu võtta

   public void lisaSoov (@RequestBody Soov soov) { //lisaSoov - meetod, mille abil sisestame uue kingisoovi andmebaasi.
        System.out.println("lisa uus soov hakkab tööle");
        System.out.println("Soovi nimi " + soov.getNimi());
        //Roheline SQL-käsk. INSERT loob tulpade nimed. VALUES lisab konkreetse soovi vastavasse tulpa.

        String sqlKask = "INSERT INTO tabel (syndmuseNimi, nimi, info, hind, kogus, link, checkBox) VALUES ('"+ soov.getSyndmuseNimi() + "', '"+ soov.getNimi() + "', '"+ soov.getInfo()+"','"+soov.getHind()+"', '"+soov.getKogus()+"','"+soov.getLink()+"','"+soov.getCheckBox()+"');";

        jdbcTemplate.execute(sqlKask);
       System.out.println("Andmebaasi sisestamine õnnestus!");

    }

    // selleks, et saaks checkboxi näitu andmebaasis muuta
    // POST /sooviStaatus?id=123
    @PostMapping ("/sooviStaatus")
    public void sooviStaatus(@RequestParam String id, @RequestParam String v6etud){
        System.out.println("sooviStaatus hakkas tööle");
        System.out.println("id on " + id + " ja v6etud on " + v6etud);
        byte v6etudInt = 0;
        if (v6etud.equals("true")) v6etudInt = 1;
        String muudaSQLi = "UPDATE tabel SET checkbox = cast("+ v6etudInt +" as bit) WHERE id = " + id;
        jdbcTemplate.execute(muudaSQLi);
        System.out.println("Andmebaasi muutmine õnnestus!");
        // UPDATE tabel SET checkbox = cast(1 as bit) WHERE id = 1
    }

    // Viib lehele, mis on backis (siin Getmappingus) ja frondis (js failis) samamoodi viidatud. Ei pea olema sama nimega kui html leht!
    // GET /syndmus?pealkiri=kana
    // Saadab info tagasi js-i, et listi soove lisades kuvaks need ka välja
    @GetMapping ("/syndmus") //get päring
    public ArrayList<Soov> prindiV2lja(@RequestParam String pealkiri) {
        System.out.println("Prindi välja käivitus!");
        ArrayList<Soov> nimekiri = new ArrayList<>(); //Arraylisti sisse tulevad tabel(id) SQL-ist
        System.out.println("Pealkiri Getmappingus: " + pealkiri);
        nimekiri = (ArrayList) jdbcTemplate.queryForList("SELECT * FROM tabel WHERE syndmuseNimi= '"+ pealkiri +"'" + " ORDER BY id ASC");
        return nimekiri;
    }



//    // Saadab info tagasi js-i kingikoti htmli kuvamiseks
//    @GetMapping ("/kingikott") //get päring
//    public ArrayList<Soov> prindiV2lja2(@RequestParam String pealkiri) {
//        System.out.println("Prindi välja käivitus!");
//        ArrayList<Soov> nimekiri = new ArrayList<>(); //Arraylisti sisse tulevad tabel(id) SQL-ist
//        System.out.println("Pealkiri Getmappingus: " + pealkiri);
//        nimekiri = (ArrayList) jdbcTemplate.queryForList("SELECT * FROM tabel WHERE syndmuseNimi= '"+ pealkiri +"'");
//        return nimekiri;
//    }
//
//
//    // Saadab info tagasi js-i lehekülg4 htmli kuvamiseks
//    @GetMapping ("/lehekylg4") //get päring
//    public ArrayList<Soov> prindiV2lja3(@RequestParam String pealkiri) {
//        System.out.println("Prindi välja käivitus!");
//        ArrayList<Soov> nimekiri = new ArrayList<>(); //Arraylisti sisse tulevad tabel(id) SQL-ist
//        System.out.println("Pealkiri Getmappingus: " + pealkiri);
//        nimekiri = (ArrayList) jdbcTemplate.queryForList("SELECT * FROM tabel WHERE syndmuseNimi= '"+ pealkiri +"'");
//        return nimekiri;
//    }
}
