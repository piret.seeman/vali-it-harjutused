package ee.kingirobot.troojahobune;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

@SpringBootApplication
public class TroojahobuneApplication implements CommandLineRunner {

	@Autowired
	JdbcTemplate jdbcTemplate;

	public static void main(String[] args) {  //Main-klass. sellepärast siia SQL tabel.
		SpringApplication.run(TroojahobuneApplication.class, args);



	}


	@Override
	public void run(String... args) throws Exception {
		String sqlKask = "CREATE TABLE if not exists tabel (id SERIAL, syndmuseNimi TEXT, nimi TEXT, info TEXT, hind TEXT, kogus TEXT, link TEXT, checkBox BIT)";
		//Defineerib ja formeerib andmebaasi tabeli.

		jdbcTemplate.execute(sqlKask);
		System.out.println("Tabel tabel on loodud");
	}
}
