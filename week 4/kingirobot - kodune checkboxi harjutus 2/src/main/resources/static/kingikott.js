
var url = "http://localhost:8080/syndmus/"

var pealkiri = getQueryVariable("pealkiri")
document.querySelector("#pealkirjakoht").innerHTML = pealkiri

// Vajalik tabeli kuvamiseks
var refreshTabel = async function() {
    console.log("refreshtabel läks käima!")
    var pealkiri = getQueryVariable("pealkiri")
    console.log("pealkiri: " + pealkiri)
    document.querySelector("#pealkirjakoht").innerHTML = pealkiri
    // Sulgudes lisatud aadress

    var andmed = await fetch(url + "?pealkiri=" + pealkiri) //APIControlleris ArrayList nimekiri
    var tabelTagasi = await andmed.json() //sama, mis APIControlleris tabel
    console.log(tabelTagasi)

    var tabelHTMLi = "" //loome tühja stringi, hakkame loopiga iga soovi kohta täitma.
    for (var soov of tabelTagasi) {
        var nimi1 = soov.nimi
        var info1 = soov.info
        var hind1 = soov.hind
        var kogus1 = soov.kogus
        var link1 = soov.link
        tabelHTMLi += "Nimi: " + nimi1 + ", Info: " + info1 + ", Hind:" + hind1 + ", Kogus: " + kogus1 + ", Link:" + link1 + "<br>" //loob iga soovi kohta ühe stringi.
    }
    // Vt selgitust @ hobune.js
    tabelHTMLi = `
        <table>
            <thread>
                <tr>
                    <th>Nimi</th>
                    <th>Info</th>
                    <th>Hind</th>
                    <th>Kogus</th>
                    <th>Link</th>
                </tr>
            </thread>
            <tbody>
                ${tabelTagasi.map(looSoov).join('')}
            </tbody>
        </table>
    `
    document.querySelector("#koht").innerHTML = tabelHTMLi //saadame ArrayListi sooviga tagasi brauserisse.
}

// Vt selgitust @ hobune.js
function looSoov(soov) {
    return `
        <tr>
            <td>${soov.nimi}</td>
            <td>${soov.info}</td>
            <td>${soov.hind}</td>
            <td>${soov.kogus}</td>
            <td><a href=${soov.link} target="_blank">${soov.link}</a></td>
        </tr>
    `
}

// Saab välja kutsuda alles pärast selle funktsiooni ära kirjeldamist
refreshTabel()

// Selleks, et listi nime kuvades jääks tühikud alles (urlis on ok kui asendub % märgiga)
function getQueryVariable(variable) {
   var query = window.location.search.substring(1);
   var vars = query.split("&");
   for (var i=0;i<vars.length;i++) {
     var pair = vars[i].split("=");
     if (pair[0] == variable) {
       return decodeURIComponent(pair[1]);
     }
   }
   alert('Query Variable ' + variable + ' not found');
 }

// Nupule vajutades liigub ajaloos sammu tagasi
var muudannimekirjaButton = document.querySelector('#muudanimekirja')
muudannimekirjaButton.onclick = function(event) {
    console.log("muudannimekirjaButton töötab!")
    history.back()
}

 var jagaButton = document.querySelector('#jaga')
     jagaButton.onclick = function(event) {
     console.log("Jagabutton töötab!")
     var APIurl = "http://localhost:8080/lehekylg4.html?pealkiri=" + pealkiri
         console.log(APIurl)
         location.href = APIurl
     }