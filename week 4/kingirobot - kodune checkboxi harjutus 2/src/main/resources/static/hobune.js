var url = "http://localhost:8080/syndmus/"

var pealkiri = getQueryVariable("pealkiri")
document.querySelector("#pealkirjakoht").innerHTML = pealkiri

document.querySelector("#form1").onsubmit = async function(e) {
    console.log("onsubmit toimib")
    e.preventDefault() //ei refreshi automaatselt, kui submit vajutada

    var nimi = document.querySelector("#nimi").value
    var info = document.querySelector("#info").value
    var hind = document.querySelector("#hind").value
    var kogus = document.querySelector("#kogus").value
    var link = document.querySelector("#link").value

    document.querySelector('#nimi').value = ""
    document.querySelector('#info').value = ""
    document.querySelector('#hind').value = ""
    document.querySelector('#kogus').value = ""
    document.querySelector('#link').value = ""

    await fetch(url, {
        method: "POST",
        body: JSON.stringify({
            syndmuseNimi: pealkiri,
            nimi,
            info,
            hind,
            kogus,
            link
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    })
  refreshTabel()
}

var refreshTabel = async function() {
    console.log("refreshtabel läks käima!")
    var pealkiri = getQueryVariable("pealkiri")
    console.log("pealkiri: " + pealkiri)
    document.querySelector("#pealkirjakoht").innerHTML = pealkiri
    // Sulgudes lisatud aadress
    var andmed = await fetch(url + "?pealkiri=" + pealkiri) //APIControlleris ArrayList nimekiri
    var tabelTagasi = await andmed.json() //sama, mis APIControlleris tabel
    console.log(tabelTagasi)

    // Koodi siit alates kuni tabeli loomiseni pole tegelikult vaja, võib ka tabeli ees defineerida lihtsalt var tabelHTMLi jms
    var tabelHTMLi = "" //loome tühja stringi, hakkame loopiga iga soovi kohta täitma.
    for (var soov of tabelTagasi) {
        var nimi1 = soov.nimi
        var info1 = soov.info
        var hind1 = soov.hind
        var kogus1 = soov.kogus
        var link1 = soov.link
        tabelHTMLi += "Nimi: " + nimi1 + ", Info: " + info1 + ", Hind:" + hind1 + ", Kogus: " + kogus1 + ", Link:" + link1 + "<br>" //loob iga soovi kohta ühe stringi.
    }
    // kirjutame üle tabelHTMLi, et see kuvaks end tabelina, valdavalt html, aga ${} sees on js
    // thead == tabeli esimene rida ehk pealkirjad; tr == praeguses kontekstis tulpade pealkirja rida
    // tbody == tabeli sisu, mis tekib iga sooviga (ehk array elemendiga)
    // ${} sees olev .map funktsiooni toimib iga soovi kohta ning kutsub välja looSoov funktsiooni
    // looSoov funktsioon defineerib iga soovi elemendid ehk asetab iga soovi atribuudid õigetesse tabeli lahtritesse
    tabelHTMLi = `
            <table>
                <thread>
                    <tr>
                        <th>Nimi</th>
                        <th>Info</th>
                        <th>Hind</th>
                        <th>Kogus</th>
                        <th>Link</th>
                    </tr>
                </thread>
                <tbody>
                    ${tabelTagasi.map(looSoov).join('')}
                </tbody>
            </table>
        `
    document.querySelector("#koht").innerHTML = tabelHTMLi //saadame ArrayListi sooviga tagasi brauserisse.
}

// Selle funktsiooniga anname tabelisse sisu - funktsioon on välja kutsutud paar rida ülal, teise funktsiooni sees
// ${} == muutuja nimi - selle sees info js-s. väljaspool htmlis
// tr == table row; td == table data
function looSoov(soov) {
    return `
        <tr>
            <td>${soov.nimi}</td>
            <td>${soov.info}</td>
            <td>${soov.hind}</td>
            <td>${soov.kogus}</td>
            <td><a href=${soov.link} target="_blank">${soov.link}</a></td>
        </tr>
    `
}

// Selleks, et listi nime kuvades jääks tühikud alles (urlis on ok kui asendub % märgiga)
function getQueryVariable(variable) {
   var query = window.location.search.substring(1);
   var vars = query.split("&");
   for (var i=0;i<vars.length;i++) {
     var pair = vars[i].split("=");
     if (pair[0] == variable) {
       return decodeURIComponent(pair[1]);
     }
   }
   alert('Query Variable ' + variable + ' not found');
 }

 var nimekirivalmisButton = document.querySelector('#nimekirivalmis')
     nimekirivalmisButton.onclick = function(event) {
     console.log("nimekirivalmisButton töötab!")
     var APIurl = "http://localhost:8080/kingikott.html?pealkiri=" + pealkiri
         console.log(APIurl)
         location.href = APIurl
     }