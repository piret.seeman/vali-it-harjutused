console.log("töötan")

var url ="http://localhost:8080/syndmus/"

var pealkiri = getQueryVariable("pealkiri")
document.querySelector("#pealkirjakoht").innerHTML = pealkiri

// Vajalik tabeli kuvamiseks
var refreshTabel = async function() {
    console.log("refreshtabel läks käima!")
    var pealkiri = getQueryVariable("pealkiri")
    console.log("pealkiri: " + pealkiri)
    document.querySelector("#pealkirjakoht").innerHTML = pealkiri
    // Sulgudes lisatud aadress

    var andmed = await fetch(url + "?pealkiri=" + pealkiri) //APIControlleris ArrayList nimekiri
    var tabelTagasi = await andmed.json() //sama, mis APIControlleris tabel
    console.log(tabelTagasi)

    var tabelHTMLi = "" //loome tühja stringi, hakkame loopiga iga soovi kohta täitma.
//    for (var soov of tabelTagasi) {
//        var nimi1 = soov.nimi
//        var info1 = soov.info
//        var hind1 = soov.hind
//        var kogus1 = soov.kogus
//        var link1 = soov.link
//        var checkBox = soov.checkBox
//        tabelHTMLi += "Nimi: " + nimi1 + ", Info: " + info1 + ", Hind:" + hind1 + ", Kogus: " + kogus1 + ", Link:" + link1 + ", " + checkBox +"<br>" //loob iga soovi kohta ühe stringi.
//    }

    // Vt selgitust @ hobune.js
    tabelHTMLi = `
        <table>
            <thead>
                <tr>
                    <th>Nimi</th>
                    <th>Info</th>
                    <th>Hind</th>
                    <th>Kogus</th>
                    <th>Link</th>
                    <th>Võetud?</th>
                </tr>
            </thead>
            <tbody id="tbody">
                ${tabelTagasi.map(looSoov).join('')}
            </tbody>
        </table>
    `
    document.querySelector("#koht").innerHTML = tabelHTMLi //saadame ArrayListi sooviga tagasi brauserisse.

    // enne märkisime tabelis tbody id="tbody", et saaks seda siin all funktsioonis kasutada
    // children == kõik tbody sees olevad read
    // array.from teeb childrenist array, et seda edasi töödelda meetoditega (children on array-like, talle ei saa kõiki meetodeid kohaldada)
    // forEach nagu for loop, käib kõik elemendid läbi - elemendid on tabeliridadest saadud read (soovid)
    // muutuja input tuleb sellest, et queryselector otsib veebilehe konkreetsest tabelireast (meie jaoks soov) üles
    // inputi (defineeritud all looSoov funktsioonis <tr> tagide vahel
    // määrame inputile onclick evendi ehk kui keegi klikib checkboxil, saadab ta serverile just selle id-ga toimunud checkboxi muutuse sisu
    var tabeliRead = document.querySelector("#tbody").children
    Array.from(tabeliRead).forEach(function(rida){
        console.log(rida.dataset.id)
        var input = rida.querySelector("input")
        console.log(input)
        input.onclick = async function(event){
            console.log(event);
            await fetch("http://localhost:8080/sooviStaatus?id=" + rida.dataset.id + "&v6etud=" + event.target.checked, { method: "POST" })
        }
    })
}

// HTMLis data-id = tulemuseks on andmete salvestamine HTMLi
function looSoov(soov) {
    console.log(soov.checkbox)
    return `
        <tr data-id=${soov.id}>
            <td>${soov.nimi}</td>
            <td>${soov.info}</td>
            <td>${soov.hind}</td>
            <td>${soov.kogus}</td>
            <td><a href=${soov.link} target="_blank">${soov.link}</a></td>
            <td><label><input type="checkbox" ${soov.checkbox ? "checked" : ""} /><span> </span></label></td>
        </tr>
    `
}

// Saab välja kutsuda alles pärast selle funktsiooni ära kirjeldamist
refreshTabel()
// Selleks, et listi nime kuvades jääks tühikud alles (urlis on ok kui asendub % märgiga)

function getQueryVariable(variable) {
   var query = window.location.search.substring(1);
   var vars = query.split("&");
   for (var i=0;i<vars.length;i++) {
     var pair = vars[i].split("=");
     if (pair[0] == variable) {
       return decodeURIComponent(pair[1]);
     }
   }
   alert('Query Variable ' + variable + ' not found');
 }
 /* var jagaButton = document.querySelector('#jaga')
      jagaButton.onclick = function(event) {
      console.log("Jagabutton töötab!")

   var jagalink ="http://localhost:8080/lehekylg4/" + "?pealkiri=" + pealkiri */