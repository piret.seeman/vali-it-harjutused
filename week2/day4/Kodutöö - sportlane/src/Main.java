import java.util.ArrayList;

public class Main {
// ainus koht, kuhu saab uusi runnereid ja skydivereid luua

    public static void main(String[] args) {
        ArrayList<Athlete> sportlased = new ArrayList<>();
        sportlased.add(new Runner("Panter", 17));
        sportlased.add(new Runner("Kass", 23));
        sportlased.add(new Runner("Kits", 21));
        sportlased.add(new Skydiver("Varblane", 25));
        sportlased.add(new Skydiver("Tuvi", 37));
        sportlased.add(new Skydiver("Pingviin", 30));

        System.out.println("Hellooooo!");

       //diver1.getClass().getDeclaredFields();

        for (Athlete sportlane:sportlased) {
            System.out.println(sportlane.nimi);
            System.out.println(sportlane.vanus);
            System.out.println(sportlane.sugu);
            System.out.println(sportlane.pikkus);
            System.out.println(sportlane.kaal);
        }
    }
}
