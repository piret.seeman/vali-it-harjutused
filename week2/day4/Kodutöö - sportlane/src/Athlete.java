// abstraktne klass = ei saa luua uut objekti ehk atleeti,
// vaid peab looma konkreetset tüüpi (runner, skydiver) atleedi.
// Siin klassis defineerime ära, mis on atleedi üldised omadused (a' la nimi jne)
// ja mida ta oskab teha (meetodid ehk siin nt perform)

public abstract class Athlete {

    String nimi;
    int vanus;
    String sugu;
    int pikkus;
    double kaal;

    public abstract void perform();

    public Athlete(String nimi, int vanus) {
        this.nimi = nimi;
        this.vanus = vanus;
    }
}
