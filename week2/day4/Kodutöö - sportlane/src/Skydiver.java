public class Skydiver extends Athlete {

    public Skydiver(String nimi, int vanus) {
        super(nimi, vanus);
    }

    @Override
    public void perform() {
        System.out.println(nimi + ": Hüppan, langen, avan varju, maandun!");
    }
}
