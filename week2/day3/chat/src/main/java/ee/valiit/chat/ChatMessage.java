package ee.valiit.chat;

public class ChatMessage {
    private int id;
    private String avatar;
    private String username;
    private String room;
    private String message;

    public ChatMessage() {
    }

    public ChatMessage(String avatar, String username, String message) {
        this.avatar = avatar;
        this.username = username;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public String getRoom() {
        return room;
    }

    public String getMessage() {
        return Security.xssFix(message);
    }

    public String getUsername() {
        return Security.xssFix(username);
    }

    public String getAvatar() {
        return Security.xssFix(avatar);
    }
}
