package ee.valiit.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Array;
import java.util.ArrayList;

@RestController
@CrossOrigin

public class APIController {

    // @Autowired on käsk, mis ühendab ära Jdbc klassi
    @Autowired
    JdbcTemplate jdbcTemplate;


    @GetMapping("/chat/{room}")
        // Get käsk, et saada SQList tulemus. Käsk on SELECT * FROM message
    ArrayList<ChatMessage> chat(@PathVariable String room) {
        // Try-catch errori vastu, mis võib tekkida, et kui päringuid palju ja serverist ei saa ühel hetkel vastust
        // Catch - et juhul kui genereerub error, siis anname tühja info vastu, et kõik kokku ei jookseks. Järgmise
        // päringuga tuleb juba õige info
        try {
            // ruumi vahetamine
            String sqlKask = "SELECT * FROM messages WHERE room='"+room+"'";
            // query järel sulgudes - SQL annab tulemuseks read, aga Java ei oska neid iseenesest mõista
            // Castime Arraylistiks
            // lambda
            // tsükkel, mille sees on meetod - ütleme, et loodaks ChatMessage. getterid
            // lõpuks tahame, et tulemus oleks ?meile loetaval kujul
            ArrayList<ChatMessage> messages = (ArrayList<ChatMessage>) jdbcTemplate.query(sqlKask, (resultSet, rownum) -> {
                // järgmine rida = mida returnib
                String avatar = resultSet.getString("avatar");
                String username = resultSet.getString("username");
                String message = resultSet.getString("message");
                return new ChatMessage(avatar, username, message);
            });
            return messages;
            // err - suvaline nimetaja
        } catch (DataAccessException err){
            System.out.println("TABLE WAS NOT READY");
            return new ArrayList();
        }
    }

    @PostMapping("/chat/{room}/new-message")
    void newMessageGen(@RequestBody ChatMessage msg, @PathVariable String room) {
        // järjekord INSERT sulgudes ja seejärel get käskudes peab mätšima!
        String sqlKask = "INSERT INTO messages (avatar, username, message, room) VALUES ('"+
                                                        msg.getAvatar() + "', '" +
                                                        msg.getUsername() + "', '" +
                                                        msg.getMessage() + "', '"+
                                                        room + "')";
        jdbcTemplate.execute(sqlKask);
    }
}
