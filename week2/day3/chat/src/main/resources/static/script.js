//Alla laadida API'st tekst. async - väga javascripti põhine
// await - kui koodi jooksutades selgub, et info võib saabuda viitega

// !! Uus muutuja, mis seab default chatroomiks general ehk selle, mis me klassis tegime
var activeChatRoom = "general";

var refreshMessages = async function() {
	//console.log, et teaks, et funktsioon läks käima ---> välja kommenteeritud
	// console.log("Refresh messages läks käima")
	// salvestasime API aadressi !! aga viisil, mis toetab õiges chatis
	var APIurl = `http://localhost:8080/chat/${activeChatRoom}`
	console.log(APIurl)
	//fetch teeb päringu serverisse (meie defineeritud aadress)
	var request = await fetch(APIurl)
	//json() käsk vormindab/formatib andmed meile mugavaks JSONiks
	var sonumid = await request.json()
	console.log(sonumid)

	// document - kogu veebilehe sisu; queryselector + '#jutt' - kolmepeale 
	//näitab, mis osa valime. innerHTML = html faili "test tekst". Määrame 
	// uue sisu ehk laseme kuvada JSONI faili

	// ÜLESANNE: kuva serverist saadud info htmlis (ehk lehel)

	document.querySelector('#jutt').innerHTML = " "
	// kommenteerisime välja pärast serveriga ühendamist--> var sonumid = json.messages
	// while tsükkel, et kogu info oleks kätte saadud - st loop kuniks sõnumeid on,
	// alustades viimasest  .pop käskluse abil
	// kuvades .pop asemel panna .shift - et kuvaks kõige uuemad sõnumid kõige all
	while (sonumid.length > 0) {
		var sonum = sonumid.shift()
		//ÜLESANNE: lisa HTMLi #jutt sisse sonum.message
		// # kui lühivorm [div id = "jutt"] asemel
		// lisada reavahe iga sõnumi vahele

		// document.querySelector('#jutt').innerHTML += "<p>" + sonum.username + ": " + sonum.message + "</p>"

		document.querySelector('#jutt').innerHTML += `<p><img src='${sonum.avatar}' height="25">${sonum.username}: ${sonum.message}</p>`

		//scrollib automaatselt kõige alla
		window.scrollTo(0,document.body.scrollHeight);
	}
}
//  vähemalt 1000 refresh rate päringu sageduseks serverist, 1000 = iga sekund
setInterval(refreshMessages, 1000)

document.querySelector('form').onsubmit = function(event) {
	event.preventDefault()
	// korjame kokku formist info, sh lisatud avatari info
	var avatar = document.querySelector('#avatar').value
	var username = document.querySelector('#username').value
	var message = document.querySelector('#message').value
	// et sõnumi kirjutamise kast läheks tühjaks kui kirjutama hakkad
	document.querySelector('#message').value = ""
	console.log(username, message)

	// POST päring postitab uue andmetüki serverisse
	var APIurl = "http://localhost:8080/chat/" + activeChatRoom + "/new-message" //on serveri poolt antud URL
	console.log(APIurl)
	fetch(APIurl, {
		method: "POST",
		// teine username ja teine message tuleb sama funktsiooni varasemast defineerimisest
		// see omakorda tuleb serveri seadistusest
		// loogelised sulud vajalikud, et meetodile JSONi rida ehk hashmap kaasa anda.
		// Defineerime selle kohe {} sulgudes ära
		body: JSON.stringify({avatar: avatar, username: username, message: message}),

		//järgnev vajalik html lehtede toimimiseripärade tõttu, mitte js eripära
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	})
}

// !! General chati funktsioon, sh esimene osa varasemast koodist (document..... ('#general')) on nüüd variable
// !! viited html-i nuppudele, et defineerida, mis juhtub kui "general chat" nupule vajutada ning millal
// !! just general chat aktiivne ja kasutusel on
var generalButton = document.querySelector('#general')
generalButton.onclick = function(event) {
    // ülejärgmise rea ainus eesmärk on printida konsooli meie jaoks, et sellele nupule vajutati
    // Nii saab esialgu kontrollida, kas nupp töötab nagu vaja. Chati toimimiseks seda vaja pole
    console.log("generalclick")
    // !! Selle käsuga saab
    generalButton.classList.add("activeChat")
    progejadButton.classList.remove("activeChat")
    activeChatRoom = "general"
}
// !! Progejad chati funktsioon - defineeritud samamoodi kui general chat, aga uute linkidega uuele chatile
var progejadButton = document.querySelector('#progejad')
progejadButton.onclick = function(event) {
    console.log("progejad-click")
    progejadButton.classList.add("activeChat")
    generalButton.classList.remove("activeChat")
    activeChatRoom = "progejad"
}