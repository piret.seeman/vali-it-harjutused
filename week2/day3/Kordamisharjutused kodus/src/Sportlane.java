public abstract class Sportlane {

    public String eesnimi;
    public String perenimi;
    public int vanus;
    public char sugu;
    public int pikkus;
    public double kaal;

    public abstract void perform();

}
