package ee.valiit.chat;

// tulid esile @RestController ja
// @CrossOrigin (lubab kõik tüüpi päringud) kirjutades
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin

public class APIController {
    ChatRoom general = new ChatRoom("general");
    ChatRoom progejad = new ChatRoom("progejad");

    @GetMapping("/chat/general")
    ChatRoom chat() {
        return general;
    }
    @PostMapping("/chat/general/new-message")
    void newMessage(@RequestBody ChatMessage msg) {
        general.addMessage(msg);
    }

    @GetMapping("/chat/progejad")
    ChatRoom chat2() {return progejad; }
    @PostMapping("/chat/progejad/new-message")
    void newMessage2(@RequestBody ChatMessage msg) {
        progejad.addMessage(msg);
    }
}
