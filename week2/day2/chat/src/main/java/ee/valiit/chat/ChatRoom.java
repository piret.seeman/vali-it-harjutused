package ee.valiit.chat;

import java.util.ArrayList;

// keerulisem ülesanne - loo teine chatroom, arraylistiga
// lihtsam ülesanne - sõnumit sisestades saadab iga kord ka profiilipildi urli, mis
// salvestub serverisse ja kuvatakse HTMLis välja

public class ChatRoom {

    public String room;

    public ArrayList<ChatMessage> messages = new ArrayList();

    public ChatRoom(String room) {
        this.room = room; // this viitab sellele konkreetsele klassile
        messages.add(new ChatMessage("https://goo.gl/abeqd2", "Pirate", "test"));
    }

    public void addMessage(ChatMessage msg) {
        messages.add(msg);
    }
}
