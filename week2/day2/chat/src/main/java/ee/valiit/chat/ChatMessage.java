package ee.valiit.chat;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ChatMessage {

    private String message;
    private String userName;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    // @Json annotatsioon peab olema enne date rida, muidu ei tööta
    private Date date;
    private String avatar;

    public ChatMessage(){
    }

    public ChatMessage(String avatar, String userName, String message) {

        this.userName = userName;
        this.message = message;
        this.date = new Date();
        this.avatar = avatar;
    }

    public String getMessage() {
        return message;
    }

    public String getUserName() {
        return userName;
    }

    public Date getDate() {
        return date;
    }

    public String getAvatar() {
        return avatar;
    }
}
