var url = "http://localhost:8080/list/"
//urli defineerimine fetchi jaoks
// ise panna mingi lisa pealehele, et saada kindlat tüüpi infot. Pealehele toimivad ka muud
// päringud, mis annavad vastuseks ka htmli, seega segaduse vältimiseks oma ruut on parem

document.querySelector("#form1").onsubmit = async function(e) { // async lisandus koos fetchiga
    console.log("onsubmit toimib")
    e.preventDefault() // siis ei refreshi automaatselt kui submit vajutada

    var nimi = document.querySelector("#nimi").value
    var vanus = document.querySelector("#vanus").value
    await fetch(url, { // lisasime await, kui alla panime juurde refreshTabeli
        method: "POST",
        // JSONi objektis "nimi: nimi" on samahea kui lihtsalt kirjutada "nimi"
        body: JSON.stringify({nimi, vanus}),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    })
    refreshTabel()
}
var refreshTabel = async function() {
    console.log("refreshTabel läks käima!!")
    var andmed = await fetch(url) // sama, mis APIControlleris nimekiri
    var isikuList = await andmed.json() // Sama, mis APIControlleris kasutajad
    console.log(isikuList)
    var isikuteHTML = "" // loome tühja stringi, hakkame seda for loopiga iga isiku kohta täitma
    for(var isik of isikuList) {
        var nimi = isik.nimi
        var vanus = isik.vanus
        isikuteHTML += "Nimi: " + nimi + ", vanus: " + vanus + "<br>" // siit tuleb iga isiku kohta tulemus üles stringi
    }
    document.querySelector("#koht").innerHTML = isikuteHTML
}



