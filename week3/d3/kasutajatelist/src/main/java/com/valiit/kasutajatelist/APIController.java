package com.valiit.kasutajatelist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@CrossOrigin
@RestController
public class APIController {

    @Autowired
    private JdbcTemplate jdbcTemplate; // tuleb alt sqlKask juurest, automaatselt. @Autowired tuleb seejärel juurde kirjutada

    @PostMapping ("/list")
    // siin tuleb defineerida meetod, mille kaudu java konverteerib fetchis küsitud andmed
    // sulgudesse annotatsioon @RequestBody, et ta teaks, et ta oskaks JS-st päringut vastu võtta
    public void handleNewListItem(@RequestBody Kasutaja kasutaja){
        System.out.println("handleNewItemList");
        System.out.println("Kasutaja nimi " + kasutaja.getNimi());
        // insert into -> tabeli nimi. Pole mõtet list lisada
        // kõik roheline osa all on SQLi käsk, INSERT ... sulgudes tulpade nimed,
        // VALUES järel sulgudes selle konkreetse kasutaja nime ja vanus õigete tulpade lahtritesse
        // esialgu VALUES sulgudesse panna ('', '') ehk ülakomad iga väärtuse kohta, seejärel edasi kirjutada
        // vanuse ümber ei pea olema '', sest tegu on int-iga
        String sqlKask = "INSERT INTO kasutajad (nimi, vanus) VALUES ('"
                + kasutaja.getNimi() + "', " + kasutaja.getVanus()+ ");";
        // jdbcTemplate (java database connectivity) objekt, mille sees on meetod nimega execute; sulgudes, mida execute'ime
        // esialgu punases, seejärel alt+enter ja tuleb ülespoole rida
        jdbcTemplate.execute(sqlKask);
        System.out.println("Sisestamine õnnestus");
    }

    @GetMapping ("/list") // get päring
    public ArrayList<Kasutaja> printListItems() {
        System.out.println("printListItem käivitus");
        ArrayList<Kasutaja> nimekiri = new ArrayList<>();
        // Arraylisti sisse tulevad kasutajad, mille saame SQList
        nimekiri = (ArrayList) jdbcTemplate.queryForList("SELECT * FROM kasutajad");
        return nimekiri;
    }
}
