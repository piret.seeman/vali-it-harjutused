// Ülesanne 5

public abstract class Country {

    private String name;
    private int population;
    private String languages;

    public Country(String name, int population, String languages) {
        this.name = name;
        this.population = population;
        this.languages = languages;
    }

    public abstract String toString();

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getLanguages() {
        return languages;
    }

    public void setLanguages(String languages) {
        this.languages = languages;
    }
}
