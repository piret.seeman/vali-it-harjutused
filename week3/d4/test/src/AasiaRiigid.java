public class AasiaRiigid extends Country {

    public AasiaRiigid(String name, int population, String languages) {
        super(name, population, languages);
    }

    @Override
    public String toString() {
        return "Ülesanne 5: "+ "Country name: '" + this.getName() + "', Population:  '" + this.getPopulation() + "', Languages: '" + this.getLanguages() + "'";
    }



}
