public class Meetodid {

    // Ülesanne 1
    public static double kaksPid(double pi){
        return pi * 2;
    }

    // Ülesanne 2
    public static boolean v6rdne(int a, int b) {
        return (a == b);
    }

    // Ülesanne 3
    // Real 17-18 on mingi loogikaviga, et õige asi välja ei tule, aga ma ei saa kuidagi jälile.
    // N-ö päris elus konsulteeriks teise paari silmadega ja küsiks nõu

    public static int[] stringiPikkused(String[] s6naMassiiv){
        int pikkused[] = new int[7];
        for (int i = 0; i < s6naMassiiv.length; i++) {
            pikkused[i] = s6naMassiiv[i].length();
        }
        return pikkused;
    }

    // Ülesanne 4
    public static byte sajand(int aastaArv){
        byte misSajand = 0;
        if (aastaArv < 1 || aastaArv > 2018) {
            misSajand = -1;
        } else {
            misSajand = (byte) (aastaArv/100 + 1);
        }
        return misSajand;
    }

}
