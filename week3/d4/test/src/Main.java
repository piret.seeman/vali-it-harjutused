public class Main {

    public static void main(String[] args) {
        System.out.println("Tere, Krister!");

        // Ülesanne 1
        double pi = 3.14159265358;
        System.out.println("Ülesanne 1: Pi kahekordne väärtus on " + Meetodid.kaksPid(pi));

        //Ülesanne 2
        System.out.println("Ülesanne 2.1: Kas muutujad on võrdsed? " + Meetodid.v6rdne(2, 2));
        System.out.println("Ülesanne 2.2: Kas muutujad on võrdsed? " + Meetodid.v6rdne(17, 93));

        // Ülesanne 3
        String[] s6naMassiiv = {"üks", "vana", "hall", "hobune", "sõitis", "üle", "kaarsilla"};
        System.out.println("Ülesanne 3: Sõnede pikkused on - " + Meetodid.stringiPikkused(s6naMassiiv));

        // Ülesanne 4
        System.out.println("Ülesanne 4.1: See aasta on sajandist " + Meetodid.sajand(0));
        System.out.println("Ülesanne 4.2: See aasta on sajandist " + Meetodid.sajand(1));
        System.out.println("Ülesanne 4.3: See aasta on sajandist " + Meetodid.sajand(128));
        System.out.println("Ülesanne 4.4: See aasta on sajandist " + Meetodid.sajand(598));
        System.out.println("Ülesanne 4.5: See aasta on sajandist " + Meetodid.sajand(1624));
        System.out.println("Ülesanne 4.6: See aasta on sajandist " + Meetodid.sajand(1827));
        System.out.println("Ülesanne 4.7: See aasta on sajandist " + Meetodid.sajand(1996));
        System.out.println("Ülesanne 4.8: See aasta on sajandist " + Meetodid.sajand(2017));

        // Ülesanne 5
        AasiaRiigid hiina = new AasiaRiigid("Hiina", 1409517397, "mandarini, mongoolia, kantoni, tiibeti, zhuangi");
        System.out.println(hiina);
    }
}
