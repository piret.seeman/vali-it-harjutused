package ee.kingirobot.troojahobune;

public class Kasutaja {

    private String kasutaja;
    private String parool;

    public Kasutaja () {
    }


    public String getKasutaja() {
        return kasutaja;
    }

    public void setKasutaja(String kasutaja) {
        this.kasutaja = kasutaja;
    }

    public String getParool() {
        return parool;
    }

    public void setParool(String parool) {
        this.parool = parool;
    }
}
