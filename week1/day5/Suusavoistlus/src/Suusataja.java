public class Suusataja {

    int stardiNumber;
    double kiirus;
    double labitudDistants;
    double dopinguKordaja; // ==>>>>>>> kodune ülesanne, et üks kümnest on väga palju kiirem.
    // null kiirusega ei tohiks keegi sõita

    public Suusataja(int i) {
        this.stardiNumber = i;
        // https://gitlab.com/aikitrumm/vali-it <<<--- pikem, keerulisem
        // https://gitlab.com/MarinaTob/vali-it-harjutused <<<---- huvitav dopingu lähenemine, traumeeritute list :)
        // - >> this.kiirus = getRandomNumberInRange(10,20);
        stardiNumber = i;
        kiirus = Math.random() * 1000; // 20km/h
        labitudDistants = 0;
    }

    public void suusata() {
        labitudDistants += kiirus / (3600);
    }

    public String toString () {
        int dist = (int) Math.round(labitudDistants *1000); // läbitud distants meetrites
        return stardiNumber + ": " + dist;
    }

    public boolean kasOnLopetanud(int koguDistants) {
        return labitudDistants >= koguDistants;
    }
}
