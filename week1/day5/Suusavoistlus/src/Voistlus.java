import java.util.ArrayList;

public class Voistlus {
    // suusavõistluses on ArrayList suusatajatega, int koguDistants  -> tsükkel (for), mis igaüks paneb suusataja suusatama (seotud meetodiga suusata());
    // pärast iga tsüklit küsime, kas keegi võitis (seotud isFinished() meetodiga)

    ArrayList<Suusataja> voistlejad;
    int koguDistants;

    // konstruktor - tunneb ära selle järgi, et on klassiga sama nimega
    public Voistlus(){
        System.out.println("Võistlus alga!");
        voistlejad = new ArrayList();
        koguDistants = 20;
        for (int i = 0; i < 6; i++) {
            voistlejad.add(new Suusataja(i));
        }
        aeg();
    }
    public void aeg() {
        // foreach
        for (Suusataja s: voistlejad) {
            s.suusata();
            boolean lopetanud = s.kasOnLopetanud(koguDistants);
            if (lopetanud) {
                System.out.println("Võitja on: " + s);
                return;
            }
        }
        System.out.println(voistlejad); // <--- prindib võistlust välja!

        try { // arvuti probleemi ennetus errorite catchimiseks
            Thread.sleep(10); // et arvuti tegevuskäiku aeglustada
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        aeg();
    }
}
