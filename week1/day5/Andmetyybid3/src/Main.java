import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
        System.out.println("hi!");

        // Ülesanne 1: (1) Loo kolm muutujat numbritega, (2) moodusta nende muutujatega lause; (3) prindi see lause välja
        int aasta = 88;
        int kuu = 9;
        int paev = 16;
        String lause = "Krister sündis " + aasta + ". aasta " + kuu + ". kuul ja " + paev + ". päeval";
        System.out.println(lause);

        // %d - täisarv, %f komakohaga arv, %s - string
        String parem = String.format("Krister sündis aastal %d. Kuu oli %d ja kuupäev %d.", aasta, kuu, paev);
        System.out.println(parem);

        // Ülesanne 2: (1) Loe kokku, mitu lampi on klassis ja pane see info HashMapi
        // (2) Loe kokku, mitu akent on klassis ja pane see info HashMapi
        // (3) Loe kokku, mitu inimest on klassis ja pane see info HashMapi

        HashMap klassiAsjad = new HashMap();
        // addides käsuga .put esimene märge võti ja teine väärtus
        // järjekorda ei saa usaldada
        klassiAsjad.put("Lambid", 11);
        klassiAsjad.put("Aknad", 5);
        klassiAsjad.put("Inimesed", 21);

        System.out.println(klassiAsjad);

        // Ülesanne 3: Mõtle välja kolm praktilist kasutust HashMapile - mis infotüübid ja olukorrad võiks olla?
        /* Kõik olukorrad, kus tuleb väärtuseid omavahel seostada - a'la
        (1) Riigid + nende riikide pealinnad;
        (2) Raamatukogus, et oleks kirjas kõik arvel olevad raamatud, nende info ja kas raamat on välja laenutatud;
        (3) Raamatupidamises, et oleks kulu tüüp ja selle suurus kirjas;
         */

        // Ülesanne 4: Prindi välja kui palju on inimesi klassis, kasutades juba loodud HashMapi
        // Väärtuse saab kätte võtit kasutades, => .get(võti)

        /*int inimesi = (int) klassiAsjad.get("Inimesed");
         * System.out.println(inimesi); */
        System.out.println(klassiAsjad.get("Inimesed"));

        // Ülesanne 5: Lisa samasse HashMappi juurde, mitu tasapinda on klassis, aga number enne ja siis String
        // näiteks (10, "Tasapinnad")

        klassiAsjad.put(10, "Tasapinnad");
        System.out.println(klassiAsjad);
        System.out.println("*********************");

        // Ülesanne 6: Loo uus HashMap, kuhu saab sisestada AINULT String:double paare. Sisesta sinna midagi
        HashMap<String, Double> uusHash = new HashMap<String, Double>();
        uusHash.put("Pähklid", 5.6);
        System.out.println(uusHash);

        // HashMap on hea valik kui jadadega ei tule enam andmetega toime

        // Ülesanne 7: switch
        int rongiNumber = 50;
        String suund = null;
        switch (rongiNumber) {
            case 50:
                suund = "Pärnu";
                break; // break vajalik, et ta lõpetaks kui ta
            case 55:
                suund = "Haapsalu";
                break;
            case 10:
                suund = "Vormsi";
                break;
        }
        System.out.println(suund);
        System.out.println("------------------------");

        //FOREACH

        int[] mingidNumbrid = new int[]{8, 4, 6, 23, 267, 247895};
        for (int i = 0; i < mingidNumbrid.length; i++) {
            System.out.println(mingidNumbrid[i]);
        }
        System.out.println("------------------------");

        // FOREACH - loopib algusest peale kogu paremalpool defineeritud jada läbi
        for (int nr : mingidNumbrid) {
            System.out.println(nr);
        }

        // Ülesanne switchiga: õpilane saab töös punkte 0-100. Kui punkte on alla 50, kukub ta töö läbi.
        // Vastasel juhul on hinne täisarvuline punktid / 20
        // 100 punkti ->> 5; 50 punkti ->> 2; 80 punkti ->> 4; 67 punkti ->> 3;

        int punkte = 92;
        if (punkte > 100 || punkte < 0){
            throw new Error();
        }
        switch ((int) Math.round(punkte / 20.0)) {
            case 5:
                System.out.println("suurepärane");
                break;
            case 4:
                System.out.println("hea");
                break;
            case 3:
                System.out.println("rahuldav");
                break;
            case 2:
                System.out.println("no-no-no");
                break;
            default:
                System.out.println("Kukkusid läbi");
        }

    }
}
