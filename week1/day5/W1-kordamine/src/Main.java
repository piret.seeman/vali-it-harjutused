import nadalapaevad.Laupaev;
import nadalapaevad.Pyhapaev;
import nadalapaevad.Reede;

public class Main {
    public static void main(String[] args) {
        System.out.println("Kordamine! :)");

        // Kordamisharjutused klassi ees

        // And + or loogika

        if (false && true || true){ // boolean tüüpi muutuja, saab olla kas true või false
            System.out.println("tõene");
        } else {
            System.out.println("väär");
        }

        // Reede on klass, koju on meetod. Kirjutasime esimesena meetodi callimise.
        // Reede punase joonega, alt+enter, tekitab package, mille ise nimetan. Seepeale tekib uue klassi fail
        // meetod koju on punase joonega, alt+ enter -> loob meetodi klassi (&faili) Reede
        Reede.koju();
        // Samasugune näide
        Laupaev.peole();

        Pyhapaev paev = new Pyhapaev();
        paev.maga();
        paev.hommik();
        // paev.uni(); kuna on private, siis ei saa välja kutsuda siin

        Pyhapaev.hommik();

    }

}
