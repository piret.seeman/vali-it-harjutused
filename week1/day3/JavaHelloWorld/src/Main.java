import java.math.BigDecimal;

/* ideaalis main klass see, kus kõiki meetodeid esile kutsutakse. Main klass võiks jääda
* samas võimalikult puhtaks ja meetodid on täpsemalt kirjeldatud klassides
*/

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World!"); // kiirkäsk sout + tab
        System.out.println("sout"); // kui teha System.out.print() - saab kuvada mitu rida

        int number = 5; // kui number on suurem kui 2mld, siis hakkab see loopima tagasi -2mld-st
        double n2 = 5.1; // long, float ja double toetavad komakohti. ei ole lõpuni täpne
        float n3 = 5.8f; //
        byte bait = 5;

        String nimi = "Piret"; //  String = objekt, võtab mälu rohkem
        char algusTaht = 'P';

        /* if (number != 5) {
        } else {
        }*/

        int liitmine = (int) (number + n2); //castimine
        System.out.println(liitmine);
        System.out.println(Math.round(10.7)); // ümardamiseks eraldi meetod

        /* fori + tab = for loopi struktuur
        while (true) {
            //number += 100;
        }*/

        // int parisNumber = (int) "4"; <-- Stringist ei saa niimoodi inti
        double parisNumber2 =  Double.parseDouble("4.1");
        int parisnumber3 = Integer.parseInt("7");

        String puuvli1 = "Banaan";
        String puuvli2 = "Apelsin";
        if (puuvli1.equals(puuvli2)){
            System.out.println("PUUVLID ON VÕRDSED!!");
        } else {
            System.out.println("EI OLE VÕRDSED");
        }

        Koer.auh();
        Koer.lausu();
        Kass.nurr();
    }

    public static int summa(int a, int b) {
        return a + b;
    }
}
