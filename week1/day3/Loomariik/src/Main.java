public class Main {
//Koduloom kui kõrgem klass, kust Koer võtab funktsiooni maga. Siin defineerime ära uue Koera ning anname talle
// funktsiooni

    public static void main(String[] args) {
        Koer pontu = new Koer();
        System.out.println("*Koer*");
        pontu.lausu();
        pontu.maga();
        pontu.ajaYles();
        String saba2 = pontu.getSaba();
        System.out.println(saba2);

        System.out.println(" ");
        System.out.println("*Kass*");

        Kass neku = new Kass();
        neku.lausu();
        neku.maga();
        neku.ajaYles();
        String saba = neku.getSaba();
        System.out.println(saba);

        System.out.println(" ");
        System.out.println("*Kilpkonn*");

        Kilpkonn kiku = new Kilpkonn();
        kiku.lausu();
        kiku.maga();
        kiku.ajaYles();
        String saba3 = kiku.getSaba();
        System.out.println(saba3);
    }
}
