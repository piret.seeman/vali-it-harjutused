console.log("Hello World!")

/* kui += siis lisab html body lõppu, kui tahad asendada, siis = "x" */
document.body.innerHTML += "<p>tere!</p>"

/* ("ol li") ehk htmlis asendas nimekirja esimese punkti*/
document.querySelector("ol li").innerHTML = "Muudetud!"

/* et Submit nupp tooks tagasi tulemuse, tulemus määratletud all sulgudes */
document.querySelector("form").onsubmit = function(e) {
	e.preventDefault()
	var nimeInput = document.querySelector("#nimi")
	console.log(nimeInput.value)
}

/* ülesanne - et submit nupu vajutuse järel tuleks see nimi veebilehele*/ 
document.querySelector("form").onsubmit = function(e){
	e.preventDefault()
	var nimeInput = document.querySelector("#nimi")

	if (nimeInput.value == "pirate") {
		document.body.innerHTML += "<p>Tere, " + nimeInput.value + "</p>"
	} else {
		document.body.innerHTML += "<p>Kes sa oled üldse, " + nimeInput.value + "</p>"
	}
}