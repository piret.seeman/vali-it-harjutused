public class Main {
    public static void main(String[] args) {

        double num1 = 5;
        double num2 = 19.0;
        double num3 = 2.74;

        System.out.println(KordusKlass.ruut(num1));
        System.out.println(KordusKlass.ruut(num2));
        System.out.println(KordusKlass.ruut(num3));

        System.out.println(KordusKlass.astendaja(5, 7));

        System.out.println("getVanus = " + KordusKlass.getVanus());

        KordusKlass kt = new KordusKlass();
        System.out.println(kt.vanus);
        kt.korrutaja(2);
        System.out.println(kt.vanus);


    }
}
