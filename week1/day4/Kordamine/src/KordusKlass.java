public class KordusKlass {

    public static int vanus = 5;

    public static int getVanus() {
        return vanus;
    }

    public static double ruut(double muutuja){
        return muutuja * muutuja;
    }

    public static int astendaja(int a, int b){
        return (int) Math.pow(a, b);
        // Math.pow => a astmel b
    }

    // static = saab kohe meetodi välja kutsuda. kui pole static, ei saa Mainis välja kutsuda
    // Näide all
    public void korrutaja (int a) {
        vanus = vanus * a;
    }


}
