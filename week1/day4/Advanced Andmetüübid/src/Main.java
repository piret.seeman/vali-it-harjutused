import java.util.ArrayList;
import java.util.Arrays;
// alt + enter kui hiir on ArrayList sõnal, saab valida, et seda importida automaatselt
// teine import tulnud Arrays.toString juures kirjutades

//// ctl + alt + L = tabulatsioon korda

public class Main {

    public static void main(String[] args) {
        //Keskseks teemaks on array ehk andmemassiiv

        int[] massiiv = new int[6];
        //primitiivsed muutujad, on kiirem ja lihtsam endal lõpuks ka lugeda
        // massiivi peamine miinus = pikkus tuleb alguses paika panna ja ei saa edaspidi muuta

        ArrayList list = new ArrayList();
        //ArrayListi loomine - mitte primitiivsed muutujad

        //Ülesanne 1: prindi välja massiiv. Massiivi sisse lisada numbrid

        System.out.println("Massiiv sisu lisamata, ehk asukoht süsteemis: " + massiiv);
        // kui printida välja massiiv, ilma sellele sisu lisamata, prindib ta välja selle asukoha süsteemis (id)

        String massiivStr = Arrays.toString(massiiv);
        //Arrays juures valida, et ta impordiks java.util.Arrayst - lisab üles uue import rea
        // Array sisu stringiks tehes saab selle stringina välja printida
        System.out.println(massiivStr);

        // Ülesanne 2: määrata kolmandal positsioonil olev number massiivis viieks
        massiiv[2] = 5;
        System.out.println(Arrays.toString(massiiv));

        // Ülesanne 3: Määra kuuendale elemendile väärtus ja prindi see välja
        massiiv[5] = 12;
        System.out.println(massiiv[5]);
        // Massiivi kogumahtu printides vaja Arrayks teha, et tulemus arusaadav oleks

        // Ülesanne 4: prindi välja viimane element, ükskõik kui pikk massiiv ka oleks
        int viimane = massiiv[massiiv.length - 1];
        System.out.println("Viimase massiivi numbri print: " + viimane);

        // Ülesanne 5: Loo uus massiiv, kus on kõik 8 numbrit kohe alguses määratud
        int[] uusMassiiv = new int[]{5, 9, 15, 92, 45, 63, 23, 38};
        System.out.println("Uus massiiv on " + Arrays.toString(uusMassiiv));

        // Ülesanne 6: prindi ükshaaval välja kõik väärtused uusMassiiv-ist
        int index = 0;
        while (index < uusMassiiv.length) {
            System.out.println(uusMassiiv[index]);
            index++;
        }
        // Ülesanne 7: teeme sama tsükli kiiremini kasutades for tsüklit
        for (int i = 0; i < uusMassiiv.length; i++) {
            System.out.println(i + ": " + uusMassiiv[i]);
        }

        // Ülesanne 8 (x2): (1) loo Stringide massiiv, mis on alguses tühi, (2) aga siis lisad keskele mingi sõne
        String[] s6neRida = new String[3];
        s6neRida[1] = "kass";
        System.out.println("Uus sõnerida: " + Arrays.toString(s6neRida));

        // Ülesanne 9: (1) loo massiiv, kus on 100 kohta. (2) sisesta sellesse massiivi loetelu numbreid 0...99ni. (3) prindi välja megamassiiv
        int[] megaMassiiv = new int[100];
        for (int i = 0; i < megaMassiiv.length; i++) {
            megaMassiiv[i] = i;
        }
        System.out.println("megaMassiivi print: " + Arrays.toString(megaMassiiv));

        // Ülesanne 10: (1) kasuta megaMassiivi massiivi, kus on numbrite jada, (2) loe mitu paarisarvu on? (3) prindi tulemus välja. kasutada nii tsüklit kui ka if lauset
        // jäägiga jagamine % (protsendi) märgiga. kui jagad kaks arvu operaatoriga %, tagastab see jäägi. x%2 tähendab, et tulemus 0 on paarisarv ja 1 ei ole
        int paarisarv = 0;
        for (int i = 0; i < megaMassiiv.length; i++) {
            if (i % 2 == 0) {
                paarisarv++;
            }
        }
        System.out.println(paarisarv);

        // Ülesanne 11: Loo ArrayList ja sisesta sinna kolm numbrit ja kaks Stringi
        ArrayList uusNimekiri = new ArrayList();
        uusNimekiri.add(4);
        uusNimekiri.add(743);
        uusNimekiri.add(178);
        uusNimekiri.add("kits");
        uusNimekiri.add("kaamera");
        System.out.println("See on ArrayList " + uusNimekiri);

        // Ülesanne 12: küsi viimasest listist välja kolmas element ja prindi välja
        // väljaprintimiseks tuleb kasutada .get lähenemist
        System.out.println(uusNimekiri.get(2));

        // Ülesanne 13: Prindi kogu list välja!
        System.out.println(uusNimekiri);

        System.out.println("     ");
        // Ülesanne 14: prindi iga element ükshaaval välja.     .get =>>> uusNimekiri.get()
        for (int i = 0; i < uusNimekiri.size(); i++) {
            System.out.println(uusNimekiri.get(i));
        }

        // Ülesanne 15: (1) Loo uus ArrayList, kus on näiteks 543 numbrit; (1.1) numbrid peavad olema Math.random() abil tehtud vahemikus 0-10
        // (2) korruta iga number viiega, (3) salvesta see uus number samale positsioonile

        ArrayList<Integer> randomList = new ArrayList();
        for (int i = 0; i < 543; i++) {
            int randomNr = (int) (Math.random() * 11);
            randomList.add(randomNr);
        }
        System.out.println("Kodutöö jada: " + randomList);

        for (int i = 0; i < randomList.size(); i++) {
            randomList.set(i, randomList.get(i) * 5);

            // alternatiiv klassis lahendades
            /* int nr = (int) randomList.get(i);
            int muudetudNr = nr * 5;
            randomList.set(i, muudetudNr); */

        }
        System.out.println("Kodutööjada*5: " + randomList);

        // esialgne versioon

        ArrayList randomList2 = new ArrayList();
        for (int i = 0; i < 543; i++) {
            int jadaNr = (int) (Math.random() * 11);
            randomList2.add(jadaNr * 5);
        }
        System.out.println("Kodutöö ver0: " + randomList2);
    }
}
