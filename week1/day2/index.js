console.log("Hommik!")

var sisend = 7
var tulemus

// < 7, siis *2; kui on >7, siis /2
// kui = 7, siis jäta samaks

// if (tingimus) {tegevus} else {tegevus2}

if (sisend == 7) {
	tulemus = sisend
} else if (sisend < 7) {
	tulemus = sisend * 2
} else {
	tulemus = sisend / 2
}

console.log("Tulemus on: " + tulemus)

/* 
Kui sõned (Stringid) on võrdsed, siis prindi (console.log) üks, kui erinevad, siis liida kokku 
ja prindi
*/

var str1 = "banaan"
var str2 = "apelsin"

if (str1 == str2) {
	console.log(str1)
} else {
	console.log(str1 + " + " + str2)
}

/* 
Meil on linnade nimekiri, aga ilma sõnata "linn". Need palun lisada.
->  nimekiri linnadest
-> while - töötab kuni muutuv tingimus on tõene. Praegusel juhul töötab see seni kuni list on 
pikem kui 0
.pop = võtab viimase nimekirjast ja salvestab ära, seejärel võtab järgmise kuni listi alguseni
*/

var linnad = ["Tallinn", "Tartu", "Valga", "Viljandi", "Narva", "Haapsalu"]
var uuedLinnad = []

while (linnad.length > 0) {
	var linn = linnad.pop()
	var uusLinn = linn + " linn"
	uuedLinnad.push(uusLinn)
}

console.log(uuedLinnad)

/*
Eralda poiste ja tüdrukute nimed
"a" lõpuga on tüdruku nimi. Googelda, kuidas küsida
while poisi nimi, lisa poisteNimed listi ja kui on tüdrukunimi, siis lisa tüdrukuteNimed listi
*/

var nimed = ["Margarita", "Mara", "Martin", "Kalev"]
var poisteNimed = []
var tydrukuteNimed = []

while (nimed.length > 0) {
	var nimi = nimed.pop()
	if (nimi.endsWith("a")) {
		tydrukuteNimed.push(nimi)
	} else {
		poisteNimed.push(nimi)
	}
}
console.log(poisteNimed, tydrukuteNimed)

// KUI ÜLDSE EI SAA ARU, KUS ON BUGI
// ctrl + klikk = mitu kursorit
// ctrl+c, kopeerid muutujad 
// ctrl+enter = uued read
// trükid console.log + ctrl+v = saad iga rea loogika välja printida


/* FUNKTSIOONID*/
/* Kirjuta algoritm, mis suudab ükskõik, mis naise/mehe nime teineteisest eristada. */

var eristaja = function(nimi) {
		if (nimi.endsWith("a")) {
			return "tüdruk"
	} else {
		return "poiss"
	}
}

var praeguneNimi = "Peeter"
var kumb = eristaja(praeguneNimi)
console.log(kumb)

//alternatiiv


/* 
	Loo funktsioon, mis tagastab vastuse küsimusele, kas tegu on numbriga.
	!isNaN(4)  <--- funktsioon is Not a Number. ! -> vastandväärtus
*/

var kasOnNumber = function(number) {
	if(!isNaN(number)) {
		return number + " is a number"
	//alternatiiv  jätta else ära ja lisada pärast ifi, et "return false"
	} else {
		return number + " is not a number"
	}
}

console.log(kasOnNumber(4))
console.log(kasOnNumber("mingi sõne"))
console.log(kasOnNumber(23536))
console.log(kasOnNumber(6.876))
console.log(kasOnNumber(null))
console.log(kasOnNumber([1, 4, 5, 6]))


/* 
	Kirjuta funktsioon, mis võtab vastu kaks numbrit ja tagastab nende summa	
*/

var numbriteSumma = function(number1, number2) {
	return number1 + number2
}
console.log(numbriteSumma(1, 2))
console.log(numbriteSumma(7, 19))


/* 

javascripti objekt != java objekt
{võti: väärtus}
console.log teine viis (inimesed.Kaarel) in javascripti 
*/
console.log("-------")

var inimesed = {
	"Kaarel": 34,
	"Margarita": 10,
	"Suksu": [3, 4, 5],
	"Krister": {
		vanus: 30, 
		sugu: true
	}
}

console.log(inimesed["Kaarel"])
console.log(inimesed.Kaarel)
console.log(inimesed.Krister.sugu)
console.log(inimesed.Suksu[1])