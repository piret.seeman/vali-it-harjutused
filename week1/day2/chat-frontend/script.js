//1. Alla laadida API'st tekst.
//async - väga javascripti põhine
// await - kui koodi jooksutades selgub, et info võib saabuda viitega

var refreshMessages = async function() {
	//tean, et funktsioon läks käima
	console.log("Refresh messages läks käima")
	// salvestasime API aadressi, on string ja salvestasime muutujasse
	var APIurl = "http://138.197.191.73:8080/chat/general"
	//fetch teeb päringu serverisse (meie defineeritud aadress)
	var request = await fetch(APIurl)
	//json() käsk vormindab/formatib andmed meile mugavaks JSONiks
	var json = await request.json()
	console.log(json)

	// document - kogu veebilehe sisu; queryselector + '#jutt' - kolmepeale 
	//näitab, mis osa valime. innerHTML = html faili "test tekst". Määrame 
	// uue sisu ehk laseme kuvada JSONI faili

	// ÜLESANNE: kuva serverist saadud info htmlis (ehk lehel)

	document.querySelector('#jutt').innerHTML = " "
	var sonumid = json.messages
	// while tsükkel, et kogu info oleks kätte saadud - st loop kuniks sõnumeid on, 
	// alustades viimasest  .pop käskluse abil
	// kuvades .pop asemel panna .shift - et kuvaks kõige uuemad sõnumid kõige all
	while (sonumid.length > 0) {
		var sonum = sonumid.shift()
		//ÜLESANNE: lisa HTMLi #jutt sisse sonum.message
		// # kui lühivorm [div id = "jutt"] asemel
		// lisada reavahe iga sõnumi vahele
		document.querySelector('#jutt').innerHTML += "<p>" + sonum.user + ": " + sonum.message + "</p>"
		//scrollib automaatselt kõige alla 
		window.scrollTo(0,document.body.scrollHeight);
	}
}
//  vähemalt 1000 refresh rate päringu sageduseks serverist, 1000 = iga sekund
setInterval(refreshMessages, 1000)

document.querySelector('form').onsubmit = function(event) {
	event.preventDefault()
	// korjame kokku formist info
	var userName = document.querySelector('#username').value
	var message = document.querySelector('#message').value
	// et sõnumi kirjutamise kast läheks tühjaks kui kirjutama hakkad
	document.querySelector('#message').value = ""
	console.log(userName, message)

	// POST päring postitab uue andmetüki serverisse
	var APIurl = "http://138.197.191.73:8080/chat/general/new-message" //on serveri poolt antud URL
	fetch(APIurl, {
		method: "POST",
		// teine username ja teine message tuleb sama funktsiooni varasemast defineerimisest
		// see omakorda tuleb serveri seadistusest
		body: JSON.stringify({user: userName, message: message}),
		//järgnev vajalik html lehtede toimimiseripärade tõttu, mitte js eripära
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		}
	})

}